int main(int argc, char **argv)
{
	ssize_t r;
	char buf[40960];

	pipeline_t *pipeline;
	pthread_t sound;

	if (argc != 2)
		return 1;

	sound_init();

	int fd = open(argv[1], O_RDONLY);

	if (fd < 0)
	{
		perror("open");
		return 1;
	}

	if (!sound_configure(16, 44100, 2))
		return 1;

	if ((pipeline = pipeline_new(4096*1000)) == NULL)
		return 1;

	main_thread = pthread_self();

	if (pthread_create(&sound, NULL, sound_thread, pipeline))
	{
		perror("pthread_create");
		return 1;
	}

	while ((r = read(fd, buf, 40960)) > 0)
	{
		pthread_mutex_lock(&sound_mutex);

		while (pipeline_write_space(pipeline) < r)
			pthread_cond_wait(&sound_cond, &sound_mutex);

		pthread_mutex_unlock(&sound_mutex);

		ssize_t left = r;

		while (left > 0)
			left -= pipeline_write(pipeline, buf + r - left, left);

		pthread_kill(sound, SIGCONT);
	}

	close(fd);

	sound_shutdown();

	return 0;
}

