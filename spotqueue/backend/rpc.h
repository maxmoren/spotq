#ifndef __RPC_H
#define __RPC_H

#include "state.h"

int rpc_init(struct state *);
void rpc_shutdown(void);

#endif

