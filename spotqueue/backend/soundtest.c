#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "sound.h"

#define BUFFER_SIZE 2048

/*

	[***  ] r = 3, bufsize = 5
	[++*  ] write buf, 2
	[*    ]
	  ^
	[**** ] left(1) + r(3) = 4

*/

int main(void)
{
	char    buffer[BUFFER_SIZE];
	ssize_t r = 0;
	size_t  w;
	int     l = 0; 

	sound_init();
	sound_configure(16, 44100, 2);
	sound_run();

	while ((r = read(STDIN_FILENO, buffer + l, BUFFER_SIZE - l)) > 0 || l > 0)
	{
		w = sound_buffer(buffer, r + l);
		l = r + l - w;

		if (l > 0)
			memmove(buffer, buffer + w, l);
	}

	sound_shutdown();

	return 0;
}

