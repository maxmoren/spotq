#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED
#define _POSIX_C_SOURCE	199309L
#include <pthread.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <libspotify/api.h>
#include <xmlrpc-c/base.h>

#include "state.h"

static pthread_mutex_t search_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t search_cond = PTHREAD_COND_INITIALIZER;

static void cb_search_complete(sp_search *result, void *userdata)
{
	sp_search **search = userdata;

	*search = result;

	pthread_cond_signal(&search_cond);
}

xmlrpc_value *
player_status(xmlrpc_env *   const envP,
              xmlrpc_value * const paramArrayP,
              void *         const serverInfo,
              void *         const channelInfo)
{
	struct state *state = serverInfo;

	(void)channelInfo;

    /* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "()");

    if (envP->fault_occurred)
        return NULL;

    return xmlrpc_build_value(envP, "i", sp_session_connectionstate(session));
}

xmlrpc_value *
player_play(xmlrpc_env *   const envP,
            xmlrpc_value * const paramArrayP,
            void *         const serverInfo,
            void *         const channelInfo)
{
	sp_session   *session = serverInfo;
	sp_link      *link;
	sp_linktype   linktype;
	sp_track     *track;

    const char *uri;

	(void)session;
	(void)channelInfo;

    /* Parse our argument array */
    xmlrpc_decompose_value(envP, paramArrayP, "(s)", &uri);

    if (envP->fault_occurred)
        return NULL;

	link = sp_link_create_from_string(uri);

	if (link == NULL)
		return xmlrpc_build_value(envP, "b", 0);

	linktype = sp_link_type(link);

	if (linktype != SP_LINKTYPE_TRACK)
	{
		fprintf(stderr, "error: link type is not a track\n");

		sp_link_release(link);

		return xmlrpc_build_value(envP, "b", 0);
	}
	else
	{
		if ((track = sp_link_as_track(link)) == NULL)
		{
			fprintf(stderr, "error: could not parse link\n");

			return xmlrpc_build_value(envP, "b", 0);
		}
	}

	sp_track_add_ref(track);
	sp_link_release(link);

	pthread_mutex_lock(&metadata_mutex);
	while (pending_track == track)
		pthread_cond_wait(&metadata_cond, &metadata_mutex);
	pthread_mutex_unlock(&metadata_mutex);

	printf("track \"%s\" added\n", sp_track_name(track));

	queue_enqueue(queue, track);

	/* Start playing music */
	start_playback();

    return xmlrpc_build_value(envP, "b", 1);
}

xmlrpc_value *
player_search(xmlrpc_env *   const envP, 
              xmlrpc_value * const paramArrayP,
              void *         const serverInfo,
              void *         const channelInfo)
{
	sp_session *session = serverInfo;
	sp_search  *search = NULL;

	char          uri[1024];
    const char   *query;
    xmlrpc_int    offset;
    xmlrpc_int    count;
    xmlrpc_value *response, *results, *item;
	xmlrpc_int    total;

	(void)channelInfo;

    /* Parse our argument array */
    xmlrpc_decompose_value(envP, paramArrayP, "(sii)", &query, &offset, &count);

	printf("searching for \"%s\"...\n", query);

    if (envP->fault_occurred)
        return NULL;

	if (sp_search_create(session, query, offset, count, 0, 0, 0, 0, cb_search_complete, &search) == NULL)
	{
		return xmlrpc_build_value(envP, "b", 0);
	}

	pthread_mutex_lock(&search_mutex);
	while (search == NULL)
		pthread_cond_wait(&search_cond, &search_mutex);
	pthread_mutex_unlock(&search_mutex);

	results = xmlrpc_array_new(envP);

	int num_tracks = sp_search_num_tracks(search);

	for (int i = 0; i < num_tracks; i++)
	{
		sp_track  *track  = sp_search_track(search, i);
		sp_album  *album  = sp_track_album(track);
		sp_artist *artist = sp_track_artist(track, 0);
		sp_link   *link   = sp_link_create_from_track(track, 0);

		sp_link_as_string(link, uri, 1024);

		const char *artist_name = sp_artist_name(artist),
		           *album_name  = sp_album_name(album),
		           *track_name  = sp_track_name(track);
		xmlrpc_int  duration    = sp_track_duration(track),
		            popularity  = sp_track_popularity(track);

		item = xmlrpc_build_value(envP, "{s:s,s:s,s:s,s:i,s:i,s:s}",
			"artist",     artist_name,
			"album",      album_name,
			"track",      track_name,
			"duration",   duration,
			"popularity", popularity,
			"link",       uri);

		sp_link_release(link);
		sp_track_release(track);

		xmlrpc_array_append_item(envP, results, item);
	}

	total = sp_search_total_tracks(search);

	const char *did_you_mean = sp_search_did_you_mean(search);

	response = xmlrpc_build_value(envP, "{s:i,s:s,s:A}",
		"total",   total,
	    "dym", did_you_mean,
		"results", results);

	printf("query \"%s\" returned %d results\n", query, total);

    /* Return our result. */
    return response;
}

xmlrpc_env *g_env;

static int
foreach_fun(void *p1, void *p2)
{
	sp_track     *track   = (sp_track *)p1;
	xmlrpc_value *results = (xmlrpc_value *)p2;
	xmlrpc_value *item;

	printf("dumping track (%p) \"%s\"...\n", (void *)track, sp_track_name(track));

	sp_album  *album  = sp_track_album(track);
	sp_artist *artist = sp_track_artist(track, 0);

	const char *artist_name = sp_artist_name(artist),
	           *album_name  = sp_album_name(album),
	           *track_name  = sp_track_name(track);
	xmlrpc_int  duration    = sp_track_duration(track),
	            popularity  = sp_track_popularity(track);

	item = xmlrpc_build_value(g_env, "{s:s,s:s,s:s,s:i,s:i}",
		"artist",     artist_name,
		"album",      album_name,
		"track",      track_name,
		"duration",   duration,
		"popularity", popularity);

	xmlrpc_array_append_item(g_env, results, item);

	return 1;
}

xmlrpc_value *
player_skip(xmlrpc_env *   const envP, 
           xmlrpc_value * const paramArrayP,
           void *         const serverInfo,
           void *         const channelInfo)
{
	sp_session *session = serverInfo;

	(void)paramArrayP;
	(void)channelInfo;

	cb_end_of_track(session);

	return xmlrpc_build_value(envP, "b", 0);	
}

xmlrpc_value *
player_queue(xmlrpc_env *   const envP, 
             xmlrpc_value * const paramArrayP,
             void *         const serverInfo,
             void *         const channelInfo)
{
	sp_session *session = serverInfo;

    xmlrpc_int    offset;
    xmlrpc_int    count;
    xmlrpc_value *response, *results;
	xmlrpc_int    total;

	(void)session;
	(void)channelInfo;

    /* Parse our argument array */
    xmlrpc_decompose_value(envP, paramArrayP, "(ii)", &offset, &count);

    if (envP->fault_occurred)
        return NULL;

	printf("dumping queue from %d to %d (total elements is %lu)\n", offset, count, queue_count(queue));

	results = xmlrpc_array_new(envP);

	g_env = envP;
	queue_foreach(queue, foreach_fun, results);

	total = queue_count(queue);

	response = xmlrpc_build_value(envP, "{s:i,s:A}",
		"total",   total,
		"results", results);

    /* Return our result. */
    return response;
}

#if 0
xmlrpc_value *
player_playlist(xmlrpc_env *   const envP, 
                xmlrpc_value * const paramArrayP,
                void *         const serverInfo,
                void *         const channelInfo)
{
	char *uri;

	xmlrpc_decompose_value(envP, paramArrayP, "(s)", &uri);

    if (envP->fault_occurred)
        return NULL;

	if ((link = sp_link_from_string(uri)) == NULL)
		return xmlrpc_build_value(envP, "i", 1);

	if (sp_link_type(link) != SP_LINKTYPE_PLAYLIST)
	{
		sp_link_release(link);
		return xmlrpc_build_value(envP, "i", 2);
	}

	if ((playlist = sp_playlist_create(link)) == NULL)
	{
		sp_link_release(link);
		return xmlrpc_build_value(envP, "i", 3);
	}

	sp_link_release(link);

/*
	for (int i = 0; i < num_tracks; i++)
	{
		
	}
*/
	sp_playlist_release(playlist);

	return xmlrpc_build_value(envP, "i", 0);
}
#endif
