#ifndef __SOUND_H
#define __SOUND_H

int sound_init(int, int, int);
int sound_samples(void);
void sound_stats(int *, int *);
size_t sound_buffer(const void *, size_t);
void sound_shutdown(void);

#endif

