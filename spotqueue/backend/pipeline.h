#ifndef __PIPELINE_H
#define __PIPELINE_H

#include <stdlib.h>

typedef struct
{
  char		 *buf;
  volatile size_t write_ptr;
  volatile size_t read_ptr;
  size_t	  size;
  size_t	  size_mask;
  int		  mlocked;

} pipeline_t;

pipeline_t *pipeline_new (size_t sz);

void pipeline_free (pipeline_t * rb);
void pipeline_reset (pipeline_t * rb);
size_t pipeline_read_space (const pipeline_t * rb);
size_t pipeline_write_space (const pipeline_t * rb);
size_t pipeline_read (pipeline_t * rb, char *dest, size_t cnt);
size_t pipeline_write (pipeline_t * rb, const char *src, size_t cnt);

#endif
