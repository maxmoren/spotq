#ifndef __RPC_METHODS
#define __RPC_METHODS

xmlrpc_value *player_status(xmlrpc_env *, xmlrpc_value * const, void * const, void * const);
xmlrpc_value *player_search(xmlrpc_env *, xmlrpc_value * const, void * const, void * const);
xmlrpc_value *player_play(xmlrpc_env *, xmlrpc_value * const, void * const, void * const);
xmlrpc_value *player_queue(xmlrpc_env *, xmlrpc_value * const, void * const, void * const);
xmlrpc_value *player_skip(xmlrpc_env *, xmlrpc_value * const, void * const, void * const);

#endif
