#include <stdlib.h>
#include "test.h"
#include "queue.h"

void *func(void *p, void *userdata)
{
	TEST(userdata == NULL);

	return p;
}

int main(void)
{
	struct queue *q;
	char *s1 = "string1",
	     *s2 = "string2",
	     *s3 = "string3";

	TEST((q = queue_new(malloc, free)) != NULL);
	TEST(queue_enqueue(q, s1) == 1);
	TEST(queue_enqueue(q, s2) == 1);
	TEST(queue_enqueue(q, s3) == 1);
	TEST(queue_peek(q) == s1);
	TEST(queue_dequeue(q) == s1);
	TEST(queue_dequeue(q) == s2);
	TEST(queue_dequeue(q) == s3);
	TEST(queue_dequeue(q) == NULL);
	TEST(queue_enqueue(q, s1) == 1);
	TEST(queue_enqueue(q, s2) == 1);
	TEST(queue_enqueue(q, s3) == 1);

	queue_reset(q);

	TEST(queue_next(q) == s1);
	TEST(queue_next(q) == s2);
	TEST(queue_next(q) == s3);

	queue_map(q, func, NULL);

	queue_destroy(q);

	return 0;
}

