#define __need_size_t
#define __need_NULL
#include <stddef.h>

struct element
{
	void *data;
	struct element *next;
};

struct queue
{
	struct element *first, *last, *current;
	void *(*alloc)(size_t);
	void (*dealloc)(void *);
	size_t count;
};

struct queue *queue_new(void *(*alloc)(size_t), void (*dealloc)(void *))
{
	struct queue *q;

	if ((q = alloc(sizeof(struct queue))) == NULL)
		return NULL;

	q->alloc   = alloc;
	q->dealloc = dealloc;
	q->first   = NULL;
	q->last    = NULL;
	q->current = NULL;
	q->count   = 0;

	return q;
}

void queue_destroy(struct queue *q)
{
	struct element *e, *next;

	for (e = q->first; e != NULL; e = next)
	{
		next = e->next;
		q->dealloc(e);
	}

	q->dealloc(q);
}

int queue_enqueue(struct queue *q, void *data)
{
	struct element *e;

	if((e = q->alloc(sizeof(struct element))) == NULL)
		return 0;

	e->data = data;
	e->next = NULL;

	if(q->last != NULL)
		q->last->next = e;

	if(q->first == NULL)
		q->first = e;

	q->last = e;
	q->count++;

	return 1;
}

void *queue_peek(struct queue *q)
{
	if (q->first == NULL)
		return NULL;

	return q->first->data;
}

void *queue_dequeue(struct queue *q)
{
	struct element *e;
	void *data;

	if (q->first == NULL)
		return NULL;

	e        = q->first;
	data     = e->data;
	q->first = e->next;

	if (e == q->current)
		q->current = NULL;

	if(e->next == NULL)
		q->last = NULL;

	q->dealloc(e);

	q->count--;

	return data;
}

void queue_map(struct queue *q, void *(*func)(void *, void *), void *data)
{
	struct element *e;

	for (e = q->first; e != NULL; e = e->next)
		e->data = func(e->data, data);
}

size_t queue_count(struct queue *q)
{
	return q->count;
}

void queue_reset(struct queue *q)
{
	q->current = q->first;
}

void *queue_next(struct queue *q)
{
	void *data;

	if (q->current == NULL)
		return NULL;

	data = q->current->data;

	q->current = q->current->next;

	return data;
}

