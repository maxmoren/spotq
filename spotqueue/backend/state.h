#ifndef _STATE_H
#define _STATE_H

struct state
{
	struct queue *queue;   /* Play queue. */
	pthread_mutex_t mutex; /* Play queue mutex. */
};

#endif

