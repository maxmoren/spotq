#ifndef _QUEUE_H
#define _QUEUE_H

#define __need_size_t
#include <stddef.h>

struct queue;

struct queue *queue_new(void *(*)(size_t), void (*)(void *));
void          queue_destroy(struct queue *);
int           queue_enqueue(struct queue *, void *);
void         *queue_dequeue(struct queue *);
size_t        queue_count  (struct queue *);
void         *queue_peek   (struct queue *);
void          queue_map    (struct queue *, void *(*)(void *, void *), void *);
void          queue_reset  (struct queue *);
void         *queue_next   (struct queue *);

#endif

