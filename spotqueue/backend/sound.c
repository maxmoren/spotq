#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <ao/ao.h>

#include "pipeline.h"
#include "sound.h"

#define SAMPLE_SIZE sizeof(int16_t)
#define BUFFER_SIZE 44100 * SAMPLE_SIZE * 2 * 2
#define CHUNK_SIZE 4096

#define TRUE 1
#define FALSE 0

#define MIN(a, b) (a < b ? a : b)

static int              g_exit      = 0;
static pthread_mutex_t  g_mutex     = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t   g_cond      = PTHREAD_COND_INITIALIZER;
static pthread_t        g_thread;
static ao_device       *g_device    = NULL;
static pipeline_t      *g_pipeline  = NULL;
static int              g_underruns = 0;

static void *sound_main(void *data)
{
	char chunk[CHUNK_SIZE];
	size_t size;

	(void)data;

	puts("sound: starting thread");

	while (g_exit == 0)
	{
		pthread_mutex_lock(&g_mutex);

		while (pipeline_read_space(g_pipeline) < SAMPLE_SIZE && g_exit == 0)
		{
			g_underruns++;
			pthread_cond_wait(&g_cond, &g_mutex);
		}

		pthread_mutex_unlock(&g_mutex);

		if (g_exit)
			break;

		size = MIN(CHUNK_SIZE, (pipeline_read_space(g_pipeline) / SAMPLE_SIZE) * SAMPLE_SIZE);
		size = pipeline_read(g_pipeline, chunk, size);

		if (ao_play(g_device, chunk, size) == 0)
		{
			fprintf(stderr, "sound: could not play chunk\n");
			return NULL;
		}
	}

	return NULL;
}

int sound_init(int bits, int rate, int channels)
{
	int driver_id;
	ao_sample_format format;

	ao_initialize();

	if ((g_pipeline = pipeline_new(BUFFER_SIZE)) == NULL)
	{
		fprintf(stderr, "sound: could not allocate memory for buffer\n");
		return FALSE;
	}

	if ((driver_id = ao_default_driver_id()) == -1)
	{
		fprintf(stderr, "sound: could not get default driver for libao\n");
		ao_shutdown();

		return FALSE;
	}

	format.bits        = bits;
	format.rate        = rate;
	format.channels    = channels;
	format.byte_format = AO_FMT_NATIVE;

	printf("sound: configuring %d channels %d bit, %d samplerate\n",
		channels, bits, rate);

	if ((g_device = ao_open_live(driver_id, &format, NULL)) == NULL)
	{
		fprintf(stderr, "sound: could not configure sound device\n");
		ao_shutdown();

		return FALSE;
	}

	pthread_create(&g_thread, NULL, sound_main, NULL);

	return TRUE;
}

void sound_stats(int *samples, int *stutter)
{
	*samples    = sound_samples();
	*stutter    = g_underruns;
	g_underruns = 0;
}

int sound_samples(void)
{
	return pipeline_read_space(g_pipeline) / SAMPLE_SIZE;
}

size_t sound_buffer(const void *buffer, size_t size)
{
	size_t written;

	if (pipeline_write_space(g_pipeline) < size)
		return 0;

	written = pipeline_write(g_pipeline, buffer, size);

	if (written > sizeof(int16_t) * 2)
		pthread_cond_signal(&g_cond);

	return written;
}

void sound_shutdown(void)
{
	if (g_device != NULL)
		ao_close(g_device);       /* Close sound device if open. */

	g_exit = 1;                   /* Set exit flag. */
	pthread_cond_signal(&g_cond); /* Poke sound thread. */

	pthread_join(g_thread, NULL); /* Wait for sound thread. */

	pipeline_free(g_pipeline);    /* Free sound buffer memory. */

	ao_shutdown();                /* Clean up libao */
}

