#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED
#define _POSIX_C_SOURCE	199309L
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <libspotify/api.h>

#include "state.h"
#include "appkey.h"
#include "sound.h"
#include "queue.h"
#include "rpc.h"

/* The main thread notify stuff */
static pthread_mutex_t g_mutex        = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  g_cond         = PTHREAD_COND_INITIALIZER;
static int             g_exit         = 0;
static int             g_track_loaded = 0;
static sp_track       *g_track        = NULL;

/* Search queue */
pthread_mutex_t      mutex_search;
struct queue        *queue_search;

/* Play queue */
pthread_mutex_t      mutex_play;
static struct queue *queue_play;

void
search(sp_session *session)
{
	struct search *search;

	pthread_mutex_lock(&mutex_search);
	search = queue_dequeue(queue_search, );
	pthread_mutex_unlock(&mutex_search);

	
}

void
start_playback(sp_session *session)
{
	struct state *state = sp_session_userdata(session);

	sp_error error;

	if (g_track == NULL)
	{
		pthread_mutex_lock(&state->mutex);
		g_track = queue_dequeue(state->queue);
		pthread_mutex_unlock(&state->mutex);

		if (g_track == NULL)
		{
			printf("pausing playback due to empty play queue\n");
			return;
		}
	}

	if (!g_track_loaded && sp_track_is_loaded(g_track))
	{
		g_track_loaded = 1;

		if ((error = sp_session_player_load(session, g_track)) == SP_ERROR_OK)
		{
			sp_session_player_play(session, 1);
			printf("starting playback\n");
		}
		else
		{
			fprintf(stderr, "error: could load next song in queue\n"
							"error: %s\n", sp_error_message(error));
			exit(1);
		}
	}
}

static void
cb_metadata_updated(sp_session *session)
{
	(void)session;

	printf("metadata updated\n");
}

static void
cb_logged_in(sp_session *session,
             sp_error error)
{
	struct state *state = sp_session_userdata(session);

	if (error != SP_ERROR_OK)
	{
		fprintf(stderr, "error: could not log in\n"
		                "error: %s\n", sp_error_message(error));
		exit(1);
	}

	printf("logged in as \"%s\"\n", USERNAME);

	/* Initialize the RPC server now that we are logged in. */
	rpc_init(state);

	/* Require high bitrate audio. */
	sp_session_preferred_bitrate(session, SP_BITRATE_320k);
}

static void
cb_logged_out(sp_session *session __attribute__((unused)))
{
	printf("logged out\n");
}

static void
cb_connection_error(sp_session *session __attribute__((unused)),
                    sp_error error)
{
	fprintf(stderr, "error: connection error\n"
	                "error: %s\n", sp_error_message(error));
}

static void
cb_get_audio_buffer_stats(sp_session *session __attribute__((unused)),
                          sp_audio_buffer_stats *stats)
{
	sound_stats(&stats->samples, &stats->stutter);
}

static int
cb_music_delivery(sp_session *session,
                  const sp_audioformat *format,
                  const void *frames,
                  int num_frames)
{
	(void)session;

	size_t frame_size = sizeof(int16_t) * format->channels;

    if (num_frames == 0)
        return 0; /* Audio discontinuity, do nothing. */

	return sound_buffer(frames, frame_size * num_frames) / frame_size;
}

static void
cb_notify_main_thread(sp_session *session)
{
	(void)session;

    pthread_cond_signal(&g_cond);
}

static void
cb_end_of_track(sp_session *session)
{
	struct state *state = sp_session_userdata(session);

	pthread_mutex_lock(&state->mutex);

	sp_track_release(g_track);
	sp_session_player_unload(session);

	g_track        = NULL;
	g_track_loaded = 0;

	pthread_mutex_unlock(&state->mutex);

	start_playback(session);
}

static const sp_session_callbacks callbacks =
{
	.logged_in              = cb_logged_in,
	.logged_out             = cb_logged_out,
	.connection_error       = cb_connection_error,
	.get_audio_buffer_stats = cb_get_audio_buffer_stats,
	.music_delivery         = cb_music_delivery,
	.notify_main_thread     = cb_notify_main_thread,
	.end_of_track           = cb_end_of_track,
	.metadata_updated       = cb_metadata_updated
};

static void
cb_signal(int sig, siginfo_t *info, void *context)
{
	(void)info;
	(void)context;

	printf("signal %d caught\n", sig);

	g_exit = 1;
	pthread_cond_signal(&g_cond);
}

int
main(void)
{
	sp_error error;
	sp_session *session;

	struct state state =
	{
		.queue = NULL,
	};

	struct sigaction act =
	{
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = &cb_signal,
	};

	/* Block all possible signals while inside signal handler. */
	sigfillset(&act.sa_mask);

	/* Register signal handlers for INT and TERM. */
	if (sigaction(SIGINT, &act, NULL) < 0 ||
	    sigaction(SIGTERM, &act, NULL) < 0)
	{
		perror("sigaction");
		return EXIT_FAILURE;
	}

	const sp_session_config config =
	{
		.api_version          = SPOTIFY_API_VERSION,
		.cache_location       = "tmp",
		.settings_location    = "tmp",
		.application_key      = appkey,
		.application_key_size = sizeof(appkey),
		.user_agent           = "test-application",
		.callbacks            = &callbacks,
		.userdata             = &state
	};

	pthread_mutex_init(&state.mutex, NULL);

	/* Start sound system. */
	sound_init(16, 44100, 2);

	if ((state.queue = queue_new(malloc, free)) == NULL)
	{
		fprintf(stderr, "error: failed to allocate play queue\n");
		return EXIT_FAILURE;
	}

	if ((error = sp_session_create(&config, &session)) != SP_ERROR_OK)
	{
		fprintf(stderr, "error: could not create session\n"
			            "error: %s\n", sp_error_message(error));

		return EXIT_FAILURE;
	}

	if ((error = sp_session_login(session, USERNAME, PASSWORD)) != SP_ERROR_OK)
	{
		fprintf(stderr, "error: session login error\n"
				        "error: %s\n", sp_error_message(error));

		return EXIT_FAILURE;
	}

	pthread_mutex_lock(&g_mutex);
	while (!g_exit)
	{
		struct timespec ts;
		int timeout;

        sp_session_process_events(session, &timeout);

        clock_gettime(CLOCK_REALTIME, &ts);

        ts.tv_sec += timeout / 1000;
        ts.tv_nsec += (timeout % 1000) * 1000000;

		pthread_cond_timedwait(&g_cond, &g_mutex, &ts);
	}
	pthread_mutex_unlock(&g_mutex);

	sp_session_release(session); /* Cleanup libspotify */
	sound_shutdown();            /* Cleanup sound and shutdown thread. */
	rpc_shutdown();              /* Cleanup RPC server and shutdown thread. */

	puts("clean exit");

	return EXIT_SUCCESS;
}

