<?php

header('Content-Type: text/html; charset=UTF-8');
header('Content-Language: en-US');

mb_internal_encoding('UTF-8');

require_once 'xmlrpc.inc.php';
require_once 'util.inc.php';
require_once 'queue.inc.php';

$rpc    = new XMLRPC('http://localhost:8000/RPC2');
$count  = 50;
$path   = filter_input(INPUT_GET, 'path', FILTER_UNSAFE_RAW);
$q      = filter_input(INPUT_GET, 'q', FILTER_UNSAFE_RAW);
$offset = filter_input(INPUT_GET, 'offset', FILTER_VALIDATE_INT);
$play   = filter_input(INPUT_GET, 'link', FILTER_UNSAFE_RAW);

list($page, $param) = explode('/', $path, 2);

if ($offset === FALSE || $offset === NULL)
	$offset = 0;

switch ($page)
{
	case 'search':
		if (!empty($q))
			$response = $rpc->{'player.search'}($q, $offset, $count);
		break;

	case 'link':
		$url = '/search/?' . http_build_query(array(
			'q'      => $q,
			'offset' => $offset
		));

		if (!empty($play))
			$rpc->{'player.link'}($play);

		header('Location: ' . $url);
		die;

	case 'skip':
		$url = '/queue/' . http_build_query(array(
			'offset' => $offset
		));

		$rpc->{'player.skip'}();

		header('Location: ' . $url);
		die;

	case 'queue':
		$response = $rpc->{'player.queue'}(0, $count);

		if ($response['total'] == 0)
			unset($response);

		break;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>spotq</title>
		<link rel="stylesheet" type="text/css" href="/static/style.css" />
	</head>
	<body>
		<div id="header">
			<h1 id="logo">spotq</h1>
			<form id="search" action="/search/" method="get">
				<input type="text" size="50" id="q" name="q" value="<?php echo htmlspecialchars($q); ?>" />
				<input type="submit" value="search" />
			</form>

			<span class="button<?php echo $page == 'queue' ? ' current' : ''; ?>"><a href="/queue/">Queue</a></span>
			<span class="button"><a href="/skip/">Skip song</a></span>
		</div>
		<div id="content">

<?php if (isset($response)) : ?>

<?php

if (!empty($response['dym']))
	echo '<p id="dym">Did you mean "<a href="/search/?q=', urlencode($response['dym']), '">', htmlspecialchars($response['dym']), '</a>"</p>', PHP_EOL;

?>

			<table id="results">
				<thead>
					<tr>
						<th id="add">&nbsp;</th>
						<th id="track">Track</th>

						<th id="artist">Artist</th>
						<th id="duration">Duration</th>
						<th id="popularity">Popularity</th>
						<th id="album">Album</th>
					</tr>
				</thead>
				<tbody>

<?php

$results = $response['results'];

if ($page == 'search')
	require 'search.tbl.php';
else if ($page == 'queue')
	require 'queue.tbl.php';

?>
				</tbody>
			</table>

			<div id="navigation">
<?php

$link_format = '<a href="/' . $page . '/?q=' . urlencode($q) . '&amp;offset=%d">%s</a>';

$pages = ceil($response['total'] / $count);
$current_page = floor($offset / $count);

if ($offset > 0)
	printf($link_format, $offset - $count, 'Previous');

for ($i = 0; $i < $pages; $i++)
	if ($i == $current_page)
		echo '<a class="current">', $i + 1, '</a>';
	else
		printf($link_format, $count * $i, $i + 1);

if ($response['total'] > $offset + $count)
	printf($link_format, $offset + $count, 'Next');

?>
			</div>

<?php else: ?>

			<div id="info">
				<h1>Search for music</h1>

				<p>Enter search terms into the search box above to find and add music to the play queue.</p>
					Examples:
					<ul>
						<li>artist:"Martika" track:"Toy Soldiers"</li>
						<li>Dr Bombay Calcutta</li>
					</ul>
				</p>
				<p>See the <a href="http://www.spotify.com/uk/about/features/searching-for-music/">Spotify manual</a> for more information about the search features.</p>
			</div>

<?php endif; ?>
		</div>
	</body>
</html>

