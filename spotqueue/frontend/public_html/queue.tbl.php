<?php

echo '<tbody>', PHP_EOL;

$first = TRUE;

foreach ($results as $result)
{
	if ($first)
	{
		$first = FALSE;
		$play = '<img src="/images/playing.png" alt="P" />';
	}
	else
		$play = '&nbsp;';

	$track      = htmlspecialchars(utf8_encode($result['track']));
	$artist     = htmlspecialchars(utf8_encode($result['artist']));
	$duration   = duration_format($result['duration']);
	$popularity = html_gauge($result['popularity']);
	$album      = htmlspecialchars(utf8_encode($result['album']));

	echo '<tr><td>', $play,       '</td>',
	         '<td>', $track,      '</td>',
		     '<td>', $artist,     '</td>',
		     '<td>', $duration,   '</td>',
		     '<td>', $popularity, '</td>',
		     '<td>', $album,      '</td></tr>', PHP_EOL;
}

echo '</tbody>', PHP_EOL;

