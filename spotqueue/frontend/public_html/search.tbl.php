<?php

echo '<tbody>', PHP_EOL;

foreach ($results as $result)
{
	$play_uri = '/link/?' . http_build_query(array(
		'q'      => $q,
		'offset' => $offset,
		'link'   => $result['link']
	));

	$play       = '<a href="' . htmlspecialchars($play_uri) .'"><img src="/images/add.png" alt="+"></a>';
	$track      = htmlspecialchars(utf8_encode($result['track']));
	$artist     = htmlspecialchars(utf8_encode($result['artist']));
	$duration   = duration_format($result['duration']);
	$popularity = html_gauge($result['popularity']);
	$album      = htmlspecialchars(utf8_encode($result['album']));

	echo '<tr><td>', $play,       '</td>',
	         '<td>', $track,      '</td>',
		     '<td>', $artist,     '</td>',
		     '<td>', $duration,   '</td>',
		     '<td>', $popularity, '</td>',
		     '<td>', $album,      '</td></tr>', PHP_EOL;
}

echo '</tbody>', PHP_EOL;

