<?php

class XMLRPCException extends Exception
{
}

class XMLRPC
{
	private $curl;

	function __construct($uri)
	{
		$this->curl = curl_init($uri);

		curl_setopt_array($this->curl, array(
			CURLOPT_PROTOCOLS      => CURLPROTO_HTTP | CURLPROTO_HTTPS,
			CURLOPT_HEADER         => FALSE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_BINARYTRANSFER => TRUE,
			CURLOPT_POST           => TRUE,
			CURLOPT_USERAGENT      => 'Sonitus/0.0.1',
			CURLOPT_HTTPHEADER     => array('Accept: text/xml', 'Content-Type: text/xml; charset=UTF-8')
		));
	}

	function __deconstruct()
	{
		curl_close($this->curl);
	}

	function __call($method, $args)
	{
		$opts = array('encoding'  => 'utf-8',
		              'escaping'  => 'markup',
		              'verbosity' => 'pretty',
		              'version'   => 'xmlrpc');

		curl_setopt($this->curl, CURLOPT_POSTFIELDS, xmlrpc_encode_request($method, $args, $opts));

		if (($response = curl_exec($this->curl)) === FALSE)
			throw new XMLRPCException('connection error');

		return xmlrpc_decode($response);
	}
}

