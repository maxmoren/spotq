<?php

/* Convenience function that escapes strings for HTML. */
function e($string)
{
	return htmlspecialchars($string,
	                        ENT_COMPAT, /* Escape only double quotes */
	                        'UTF-8',    /* Use unicode (does nothing) */
	                        TRUE);      /* No double-encode detection */
}

function duration_format($ms)
{
	$h = floor($ms / 3600000);
	$m = floor($ms / 60000) - $h * 60;
	$s = round($ms / 1000) - $m * 60 - $h * 3600;

	return str_pad($h, 2, '0', STR_PAD_LEFT) . ':' . 
	       str_pad($m, 2, '0', STR_PAD_LEFT) . ':' .
	       str_pad($s, 2, '0', STR_PAD_LEFT);
}

function html_gauge($percent)
{
	return '<div title="' . $percent . '%" class="gauge-bg"><div style="width: ' . round($percent / 20) * 20 . '%" class="gauge-fg"></div></div>';
}

