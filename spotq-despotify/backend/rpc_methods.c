#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <xmlrpc-c/base.h>

#include "spotq.h"
#include "queue.h"

static pthread_mutex_t search_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t search_cond = PTHREAD_COND_INITIALIZER;

xmlrpc_value *
player_link(xmlrpc_env *   const envP,
            xmlrpc_value * const paramArrayP,
            void *         const serverInfo,
            void *         const channelInfo)
{
	struct sq_link *link = malloc(sizeof(struct sq_link));

	(void)serverInfo;
	(void)channelInfo;

    /* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(s)", &link->uri);

    if (envP->fault_occurred)
        return NULL;

	if (!queue_enqueue(link_queue, link))
	{
		free(link);
		return xmlrpc_build_value(envP, "b", FALSE);
	}

	/* Notify main to add link to play queue. */
	notify_main();

	return xmlrpc_build_value(envP, "b", TRUE);
}

xmlrpc_value *
player_search(xmlrpc_env *   const envP,
              xmlrpc_value * const paramArrayP,
              void *         const serverInfo,
              void *         const channelInfo)
{
	struct sq_search *search = malloc(sizeof(struct sq_search));

	if (search == NULL)
	{
		perror("malloc");
		return NULL;
	}

	char *in_query;
	xmlrpc_int in_offset;
	xmlrpc_int in_count;
	xmlrpc_value *results;

	(void)serverInfo;
	(void)channelInfo;

    /* Parse our argument array. */
    xmlrpc_decompose_value(envP, paramArrayP, "(sii)", &in_query, &in_offset, &in_count);

    if (envP->fault_occurred)
        return NULL;

	/* Create result array. */
	results = xmlrpc_array_new(envP);

	/* Initialize the search. */
	struct sq_search *s = sq_search_new(in_query, in_offset, in_count, &search_cond);

	printf("new search. queueing...\n");

	if (!queue_enqueue(search_queue, search))
	{
		free(search);
		return NULL;
	}

	printf("search \"%s\" queued; waiting for results\n", in_query);

	/* Notify main thread to process search. */
	notify_main();

	/* Lock and wait for our search to be completed. */
	pthread_mutex_lock(&search_mutex);

	while (!search->complete)
		pthread_cond_wait(&search_cond, &search_mutex);

	pthread_mutex_unlock(&search_mutex);

	printf("completed search for \"%s\", %d tracks returned\n", in_query, search->num_tracks);

	if (search->error)
		fputs("error: search failed\n", stderr);
	else
	{
		for (int i = 0; i < search->num_tracks; i++)
		{
			xmlrpc_value *item;
			struct sq_track *t = &search->tracks[i];

			item = xmlrpc_build_value(envP, "{s:s,s:s,s:s,s:s,s:i,s:i}",
				"artist",     sq_track_get_artist(t),
				"album",      sq_track_get_album(t),
				"title",      sq_track_get_title(t),
				"link",       sq_track_get_link(t),
				"duration",   sq_track_get_duration(t),
				"popularity", sq_track_get_popularity(t));

			sq_track_destroy(t);

			xmlrpc_array_append_item(envP, results, item);
		}

		sq_search_destroy(s);
	}

    xmlrpc_value *response = xmlrpc_build_value(envP, "{s:i,s:s,s:A}",
		"total",   search->tot_tracks,
		"dym",     search->dym,
		"results", results);

	if (search->dym != NULL)
		free(search->dym);
	free(search);

	return response;
}

#if 0
static void
foreach_fun(size_t index, void *p1, void *p2)
{
	xmlrpc_env env;
	struct track *t = (struct track *)p1;
	struct track_info *ti = t->track_info;
	xmlrpc_value *results = (xmlrpc_value *)p2;
	xmlrpc_value *item;

	(void)index;

	xmlrpc_env_init(&env);

	item = xmlrpc_build_value(&env, "{s:s,s:s,s:s,s:i,s:i}",
		"artist",     ti->artist,
		"album",      ti->album,
		"track",      ti->track,
		"duration",   ti->duration,
		"popularity", ti->popularity);

	if (env.fault_occurred)
	{
		fputs("error: xmlrpc_build_value failed\n", stderr);
		xmlrpc_env_clean(&env);
		return;
	}

	xmlrpc_array_append_item(&env, results, item);

	if (env.fault_occurred)
		fputs("error: xmlrpc_array_append_item failed\n", stderr);

	xmlrpc_env_clean(&env);
}

xmlrpc_value *
player_queue(xmlrpc_env *   const envP, 
             xmlrpc_value * const paramArrayP,
             void *         const serverInfo,
             void *         const channelInfo)
{
	sp_session *session = serverInfo;

    xmlrpc_int    in_offset;
    xmlrpc_int    in_count;
    xmlrpc_value *response, *results;
	xmlrpc_int    total;

	(void)session;
	(void)channelInfo;

    /* Parse our argument array */
    xmlrpc_decompose_value(envP, paramArrayP, "(ii)", &in_offset, &in_count);

    if (envP->fault_occurred)
        return NULL;

	results = xmlrpc_array_new(envP);

	queue_foreach(play_queue, foreach_fun, results);

	total = queue_count(play_queue);

	response = xmlrpc_build_value(envP, "{s:i,s:A}",
		"total",   total,
		"results", results);

    /* Return our result. */
    return response;
}

xmlrpc_value *
player_skip(xmlrpc_env *   const envP, 
            xmlrpc_value * const paramArrayP,
            void *         const serverInfo,
            void *         const channelInfo)
{
	(void)paramArrayP;
	(void)serverInfo;
	(void)channelInfo;

	/* Make player stop playing the current track. */
	g_end_of_track = TRUE;

	/* Poke main. */
	notify_main();

	return xmlrpc_build_value(envP, "b", TRUE);
}
#endif

