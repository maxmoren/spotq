#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED
#define _POSIX_C_SOURCE	199309L
#include <pthread.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <despotify.h>

#include "spotq.h"
#include "appkey.h"
#include "queue.h"
#include "sound.h"
#include "rpc.h"

/* High bitrate */
#define HQ true

/* Action queues. */
struct queue *link_queue   = NULL;
struct queue *search_queue = NULL;
struct queue *play_queue   = NULL;

/* Global status. */
static int g_exit      = FALSE;
static int g_playing   = FALSE;

int g_end_of_track = FALSE;

pthread_mutex_t notify_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t notify_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t g_playback_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_playback_cond = PTHREAD_COND_INITIALIZER;

/* Track structure for abstraction. */
struct sq_track
{
	struct track *track; /* The real despotify track structure. */
};

struct sq_search *
sq_search_new(const char *query, int offset, int count, pthread_cond_t *cond)
{
	struct sq_search search = malloc(sizeof(struct sq_search *));

	if (search == NULL)
		return NULL;

	s->query    = query;
	s->offset   = offset;
	s->count    = count;
	s->cond     = cond;
	s->complete = FALSE;
	s->error    = FALSE;
	s->results  = results;
}

struct sq_track *
sq_search_get(struct sq_search *s, int index)
{
	return s == NULL ? NULL : &s->tracks[index];
}

void
sq_track_destroy(struct sq_track *t)
{
	if (t == NULL)
		return;

	despotify_free_track(t->track);
	free(t);
}

const char *
sq_track_get_artist(struct sq_track *t)
{
	return t == NULL ? NULL : t->track->artist->name;
}

const char *
sq_track_get_title(struct sq_track *t)
{
	return t == NULL ? NULL : t->track->title;
}

const char *
sq_track_get_album(struct sq_track *t)
{
	return t == NULL ? NULL : t->track->album;
}

const unsigned char *
sq_track_get_link(struct sq_track *t)
{
	return t == NULL ? NULL : t->track->track_id;
}

int
sq_track_get_popularity(struct sq_track *t)
{
	return t == NULL ? 0 : (int)(t->track->popularity * 100.0);
}

int
sq_track_get_duration(struct sq_track *t)
{
	return t->track->length;
}

static void
signal_handler(int sig, siginfo_t *info, void *context)
{
	(void)info;
	(void)context;

	printf("signal %d caught\n", sig);

	g_exit = TRUE;
	notify_main();
}

static void sess_callback(struct despotify_session* ds, int signal, void *data, void* callback_data)
{
	(void)data;
	(void)ds;
	(void)callback_data;

	switch (signal) {
		case DESPOTIFY_NEW_TRACK:
			puts("new track event");
			break;

		case DESPOTIFY_END_OF_PLAYLIST:
			{				
				sq_track_destroy(queue_dequeue(play_queue));

				puts("end of playlist (or track)");
				g_playing = FALSE;
				notify_main();
			}
			break;
	}
}

void
notify_main(void)
{
	pthread_cond_signal(&notify_cond);
}

void *playback_main(void *data)
{
	struct despotify_session *session = (struct despotify_session *)data;
	struct pcm_data pcm;
	int r, w;

	for (;;)
	{
		while ((r = despotify_get_pcm(session, &pcm)) == 0 && pcm.len > 0)
		{
			w = 0;

			while (w < pcm.len)
			{
				w += sound_buffer(pcm.buf + w, pcm.len - w);

				if (w < pcm.len)
					usleep(10000); /* Suspend for a while. */
			}
		}

		fputs("playback thread suspending.\n", stderr);

		pthread_mutex_lock(&g_playback_mutex);
		while (!g_playing)
			pthread_cond_wait(&g_playback_cond, &g_playback_mutex);
		pthread_mutex_unlock(&g_playback_mutex);
	}

	return NULL;
}

int
main(void)
{
	struct despotify_session *session;

	struct sigaction act =
	{
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = &signal_handler,
	};

	/* Block all possible signals while inside signal handler. */
	sigfillset(&act.sa_mask);

	/* Register signal handlers for INT and TERM. */
	if (sigaction(SIGINT, &act, NULL) < 0 ||
	    sigaction(SIGTERM, &act, NULL) < 0)
	{
		perror("sigaction");
		return EXIT_FAILURE;
	}

	if ((play_queue = queue_new(malloc, free))   == NULL ||
	    (link_queue = queue_new(malloc, free))   == NULL ||
	    (search_queue = queue_new(malloc, free)) == NULL)
	{
		fputs("error: malloc failed\n", stderr);
		return EXIT_FAILURE;
	}

	if (!despotify_init())
	{
		fputs("error: failed to initialize despotify\n", stderr);
		return EXIT_FAILURE;
	}

	if (!(session = despotify_init_client(sess_callback, /* Session callback */
	                                      NULL,          /* User data */
	                                      HQ,            /* High bitrate */
	                                      true           /* Enable cache */
	                                     )))
	{
		fputs("error: failed to create session\n", stderr);
		return EXIT_FAILURE;
	}

	if (!despotify_authenticate(session, USERNAME, PASSWORD))
	{
		fprintf(stderr, "error: could not log in: %s\n",
			despotify_get_error(session));
		despotify_cleanup();
		return EXIT_FAILURE;
	}

	sound_init(16, 44100, 2, 1);
	rpc_init();

	pthread_t playback_thread;
	pthread_create(&playback_thread, NULL, playback_main, session);

	puts("logged in and entering main loop");

	while (!g_exit)
	{
		/* Process queued searches. */

		struct sq_search *search;

		while ((search = queue_dequeue(search_queue)) != NULL)
		{
			/* We can't search with an offset without first searching for at least one track at offset 0.
			   Because of this bullshit we need to call despotify_search followed by a despotify_search_more. */

			struct search_result *results;

			results = despotify_search(session, search->query, search->count);
			// results = despotify_search_more(session, results, search->offset, search->count);

			if (results != NULL)
			{
				search->tot_tracks = results->total_tracks;
				search->dym        = results->suggestion != NULL ? strdup((char *)results->suggestion) : NULL;
				search->num_tracks = results->playlist->num_tracks;

				search->tracks     = malloc(sizeof(struct sq_track) * search->num_tracks);

				if (search->tracks != NULL)
				{
					struct track *track = results->playlist->tracks;

					for (int i = 0; i < search->num_tracks; i++)
					{
						struct sq_track *t = &search->tracks[i];
						t->track = track;

						track = track->next;
					}
				}
				else
					search->error = TRUE;

				despotify_free_search(results); /* TODO: Maybe not... */
			}
			else
				search->error = TRUE;

			search->complete = TRUE;
			pthread_cond_broadcast(search->cond);
		}

		/* Process link queue. */
		
		struct sq_link *link;

		while ((link = queue_dequeue(link_queue)))
		{
			printf("A link with URI \"%s\" was received.\n", link->uri);

			struct track *track = despotify_get_track(session, link->uri);

			free(link->uri);
			free(link);

			if (track != NULL)
			{
				struct sq_track *t = malloc(sizeof(struct sq_track));

				if (t != NULL)
				{
					t->track = track;
					queue_enqueue(play_queue, t);

					printf("%s - %s queued\n", track->artist->name, track->title);
				}
			}
		}

		struct sq_track *t;

		if (!g_playing && (t = queue_peek(play_queue)))
		{
			despotify_play(session, t->track, false);
			puts("starting playback");

			g_playing = TRUE;
			pthread_cond_signal(&g_playback_cond);
		}

		pthread_mutex_lock(&notify_mutex);
		pthread_cond_wait(&notify_cond, &notify_mutex);
		pthread_mutex_unlock(&notify_mutex);
	}

	puts("normal exit");

	return EXIT_SUCCESS;
}

