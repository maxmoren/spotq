#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <ao/ao.h>
/* #include <samplerate.h> */

#include "pipeline.h"
#include "sound.h"

#define CHUNK_SIZE 4096

#define TRUE 1
#define FALSE 0

#define MIN(a, b) (a < b ? a : b)

static int              g_exit         = 0;
static int              g_play         = 0;
static int              g_started      = 0;

static pthread_mutex_t  g_mutex        = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t   g_cond         = PTHREAD_COND_INITIALIZER;

static pthread_t        g_thread;
static ao_device       *g_device       = NULL;
static pipeline_t      *g_pipeline     = NULL;

static int              g_sample_size;

/* Consumer thread main function. */

static void *sound_main(void *data)
{
	char chunk[CHUNK_SIZE];
	size_t size;

	(void)data;

	puts("sound: starting thread");

	while (!g_exit)
	{
		if (!pipeline_read_space(g_pipeline))
		{
			fputs("sound: buffer underrun\n", stderr);

			pthread_mutex_lock(&g_mutex);		
			g_play = 0;

			while (!g_play && !g_exit)
				pthread_cond_wait(&g_cond, &g_mutex);

			pthread_mutex_unlock(&g_mutex);

			if (g_exit)
				break;
		}

		size = MIN(CHUNK_SIZE, pipeline_read_space(g_pipeline));
		size = pipeline_read(g_pipeline, chunk, size);

		if (ao_play(g_device, chunk, size) == 0)
		{
			fprintf(stderr, "sound: could not play chunk\n");
			return NULL;
		}
	}

	puts("sound: exiting thread");

	return NULL;
}

/* Function that initializes and configures the output sound device. */

int sound_init(int bits, int rate, int channels, int seconds)
{
	int driver_id;

	ao_sample_format format =
		{
			.bits        = bits,
			.rate        = rate,
			.channels    = channels,
			.byte_format = AO_FMT_LITTLE,
		};

	ao_initialize();

	g_sample_size = bits / 8 * channels;

	if ((g_pipeline = pipeline_new(g_sample_size * rate * seconds)) == NULL)
	{
		fprintf(stderr, "sound: could not allocate memory for buffer\n");
		sound_shutdown();	
	
		return FALSE;
	}

	if ((driver_id = ao_default_driver_id()) == -1)
	{
		fprintf(stderr, "sound: could not get default driver for libao\n");
		sound_shutdown();

		return FALSE;
	}

	printf("sound: configuring %d channels %d bit, %d samplerate\n",
		format.channels, format.bits, format.rate);

	if ((g_device = ao_open_live(driver_id, &format, NULL)) == NULL)
	{
		fprintf(stderr, "sound: could not configure sound device\n");
		sound_shutdown();

		return FALSE;
	}

	if (pthread_create(&g_thread, NULL, sound_main, NULL) != 0)
	{
		fprintf(stderr, "sound: could not create consumer thread\n");
		sound_shutdown();

		return FALSE;
	}

	g_started = 1;

	return TRUE;
}

/* Resample and queue samples into the playback buffer. */

size_t sound_buffer(const void *buffer, size_t size)
{
	size_t written;

	if (!g_started)
		return 0;

	written = pipeline_write(g_pipeline, buffer, size);

	/* TODO: Resample. */
	/* divide with 0x8000, multiply by 0x7FFF */

	if (!g_play && pipeline_read_space(g_pipeline) > CHUNK_SIZE)
	{
		g_play = 1;
		puts("sound: waking up");
		pthread_cond_signal(&g_cond);
	}

	return written;
}

void sound_shutdown(void)
{
	if (g_started)
	{
		g_exit = 1;
		pthread_cond_signal(&g_cond);

		pthread_join(g_thread, NULL);
	}

	if (g_device != NULL)
		ao_close(g_device);

	if (g_pipeline != NULL)
		pipeline_free(g_pipeline);

	ao_shutdown();

	g_started = 0;

	puts("sound: clean shutdown");
}

