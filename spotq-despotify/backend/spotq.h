#ifndef _SPOTQ_H
#define _SPOTQ_H

#include <pthread.h>
#include <libspotify/api.h>

#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

struct sq_track;

/* struct track
{
	int loaded;
	struct track_info *track_info;
	sp_track *track;
}; */

struct sq_search
{
	char *query;
	char *dym;
	int offset;
	int count;
	int tot_tracks;
	int num_tracks;
	struct sq_track *tracks;
	int error;
	int complete;
	pthread_cond_t *cond;
};

struct sq_link
{
	char *uri;
};

void notify_main(void);

extern struct queue *link_queue;
extern struct queue *search_queue;
extern struct queue *play_queue;

void					sq_track_destroy		(struct sq_track *t);

const char *			sq_track_get_artist		(struct sq_track *t);
const char *			sq_track_get_title		(struct sq_track *t);
const char *			sq_track_get_album		(struct sq_track *t);
const unsigned char *	sq_track_get_link		(struct sq_track *t);
int 					sq_track_get_popularity	(struct sq_track *t);
int						sq_track_get_duration	(struct sq_track *t);

#endif

