#ifndef __SOUND_H
#define __SOUND_H

int    sound_init     (int, int, int, int);
void   sound_shutdown (void);

size_t sound_buffer   (const void *, size_t);

#endif

