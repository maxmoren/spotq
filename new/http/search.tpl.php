<?php require 'head.tpl.php'; ?>

<?php if (!empty($dym)): ?>
<p id="dym">Did you mean <a href="<?php e($dym_uri); ?>"><?php echo e($dym); ?></a></p>
<?php endif; ?>

<?php if (!empty($results)): ?>

<table id="results">
	<tr>
		<th id="add">&nbsp;</th>
		<th id="title">Title</th>
		<th id="artist">Artist</th>
		<th id="duration">Duration</th>
		<th id="popularity">Popularity</th>
		<th id="album">Album</th>
	</tr>
<?php foreach ($results as $result): ?>
	<tr>
		<td><a href="<?php echo e($result['play_uri']); ?>" title="Enqueue track"><b>+</b></a></td>
		<td><?php echo e($result['title']);      ?></td>
		<td><?php echo e($result['artist']);     ?></td>
		<td><?php echo e($result['duration']);   ?></td>
		<td><?php echo e($result['popularity']); ?>%</td>
		<td><?php echo e($result['album']);      ?></td>
	</tr>
<?php endforeach; ?>
</table>

<?php require 'navigation.tpl.php'; ?>

<?php else: ?>

<div id="info"><h1>No results</h1><p>Your search did not return a single result. Try something else.</p></div>

<?php endif; ?>

<?php require 'bottom.tpl.php'; ?>

