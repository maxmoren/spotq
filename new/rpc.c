#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* xmlrpc */
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/abyss.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/server_abyss.h>

/* libdespotify */
#include <despotify.h>

#include "queue.h"

static xmlrpc_registry *registry = NULL;
static xmlrpc_env env;

/* Declarations. */
extern struct despotify_session *session;
extern struct queue *play_queue;
extern struct queue *play_queue;
void poke_playback(void);
void skip_track(void);

static void play_queue_foreach(size_t index, void *data, void *userdata)
{
	(void)index;

	struct track *t = (struct track *)data;
	xmlrpc_value *results = (xmlrpc_value *)userdata;

	xmlrpc_value *item = xmlrpc_build_value(&env, "{s:s,s:s,s:s,s:s,s:i,s:i}",
		"artist",     t->artist->name,
		"album",      t->album,
		"title",      t->title,
		"link",       t->track_id,
		"duration",   t->length,
		"popularity", (int)(t->popularity * 100.0));

	if (item)
		xmlrpc_array_append_item(&env, results, item);
}

xmlrpc_value *
player_queue(xmlrpc_env *   const env,
             xmlrpc_value * const params,
             void *         const server_info,
             void *         const channel_info)
{
	xmlrpc_value *results  = NULL;

	(void)params;
	(void)server_info;
	(void)channel_info;

	results = xmlrpc_array_new(env);

	if (results)
	{
		queue_foreach(play_queue, play_queue_foreach, results);
		return results;
	}

	return NULL;
}

xmlrpc_value *
player_skip(xmlrpc_env *   const env,
            xmlrpc_value * const params,
            void *         const server_info,
            void *         const channel_info)
{
	(void)params;
	(void)server_info;
	(void)channel_info;

	skip_track();

	return xmlrpc_build_value(env, "b", 1);
}

xmlrpc_value *
player_search(xmlrpc_env *   const env,
              xmlrpc_value * const params,
              void *         const server_info,
              void *         const channel_info)
{
	char *query;
	xmlrpc_int count;
	xmlrpc_value *results  = NULL;
	xmlrpc_value *response = NULL;

	(void)server_info;
	(void)channel_info;

    xmlrpc_decompose_value(env, params, "(si)", &query, &count);

	if (env->fault_occurred)
		return NULL;

	struct search_result *sr = despotify_search(session, query, count);

	if (sr != NULL)
	{
		results = xmlrpc_array_new(env);

		if (results)
			for (struct track *t = sr->playlist->tracks; t; t = t->next)
			{
				if (!t->artist->name || !t->album || !t->title || !t->track_id)
					continue;

				xmlrpc_value *item = xmlrpc_build_value(env, "{s:s,s:s,s:s,s:s,s:i,s:i}",
					"artist",     t->artist->name,
					"album",      t->album,
					"title",      t->title,
					"link",       t->track_id,
					"duration",   t->length,
					"popularity", (int)(t->popularity * 100.0));

				if (item)
					xmlrpc_array_append_item(env, results, item);
			}

		response = xmlrpc_build_value(env, "{s:i,s:s,s:A}",
			"total",   sr->total_tracks,
			"dym",     sr->suggestion,
			"results", results);

		despotify_free_search(sr);

		return response;
	}
	else
		return NULL;
}

xmlrpc_value *
player_play(xmlrpc_env *   const env,
            xmlrpc_value * const params,
            void *         const server_info,
            void *         const channel_info)
{
	char *id;

	(void)server_info;
	(void)channel_info;

    xmlrpc_decompose_value(env, params, "(s)", &id);

	if (env->fault_occurred)
		return NULL;

	struct track *track = despotify_get_track(session, id);

	if (track != NULL)
	{
		printf("\"%s - %s\" enqueued\n", track->title, track->artist->name);

		queue_enqueue(play_queue, track);
		poke_playback();
		return xmlrpc_build_value(env, "b", TRUE);
	}

	return NULL;
}

void rpc_exit_error(const char *what)
{
	if (env.fault_occurred)
	{
		fprintf(stderr, "rpc: %s failed: %s\n", what, env.fault_string);
		exit(EXIT_FAILURE);
	}
}

void shutdown_rpc(void)
{
	if (registry != NULL)
		xmlrpc_registry_free(registry);

	xmlrpc_env_clean(&env);

	puts("rpc: exit");
}

void rpc_main(void)
{
	xmlrpc_server_abyss_parms serverparm;
	struct xmlrpc_method_info3 const methods[] = {
		{ .methodName = "player.search", .methodFunction = &player_search },
		{ .methodName = "player.play",   .methodFunction = &player_play   },
		{ .methodName = "player.queue",  .methodFunction = &player_queue  },
		{ .methodName = "player.skip",   .methodFunction = &player_skip   },
	};

	xmlrpc_env_init(&env);
	rpc_exit_error("env_init");

	atexit(shutdown_rpc);

	registry = xmlrpc_registry_new(&env);
	rpc_exit_error("registry_new");

	for (int i = 0; i < 4; i++)
	{
		xmlrpc_registry_add_method3(&env, registry, &methods[i]);
		rpc_exit_error("registry_add");
	}

    serverparm.config_file_name = NULL;
    serverparm.registryP = registry;
    serverparm.port_number = 8080;
    serverparm.log_file_name = "/tmp/xmlrpc.log";

	puts("rpc: starting server");

    xmlrpc_server_abyss(&env, &serverparm, XMLRPC_APSIZE(log_file_name));
	rpc_exit_error("server_abyss");
}

