/* gcc -ggdb -W -Wall -pedantic -std=c99 spotq.c rpc.c queue.c -o spotq -lxmlrpc -lxmlrpc_server_abyss -lao -ldespotify */

#define _POSIX_C_SOURCE	199309L
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

/* libao */
#include <ao/ao.h>

/* libdespotify */
#include <despotify.h>

#include "queue.h"

#define USERNAME "PinGoo"
#define PASSWORD "Ifohj8Nu"

struct despotify_session *session = NULL;

static ao_device *sound_device = NULL;

static pthread_t playback_thread;
static pthread_mutex_t playback_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t playback_cond = PTHREAD_COND_INITIALIZER;

enum playback_state
{
	PLAYER_PLAYING,      /* We are currently playing the top of the queue. */
	PLAYER_END_OF_TRACK, /* We are at the end of the top of the queue. */
	PLAYER_STOPPED       /* We are idle. */

} state = PLAYER_STOPPED;

static int running = 1;
static int sound_initialized = 0;

struct queue *play_queue = NULL;

void rpc_main(void);

void shutdown_sound(void)
{
	if (sound_device != NULL)
		ao_close(sound_device);

	ao_shutdown();

	if (sound_initialized)
		pthread_join(playback_thread, NULL);

	puts("sound: exit");
}

static void next_track(void)
{
	struct track *track, *old_track = NULL;

	if (state == PLAYER_PLAYING)
	{
		puts("currently playing, not switching track");
		return;
	}
	else if (state == PLAYER_END_OF_TRACK)
		old_track = queue_dequeue(play_queue);

	track = queue_peek(play_queue);

	if (track != NULL)
	{
		puts("starting next track");

		/* Switch track. */
		despotify_play(session, track, false);

		if (state == PLAYER_END_OF_TRACK)
			despotify_free_track(old_track);

		/* Set state to playing to start playback. */
		state = PLAYER_PLAYING;
	}
	else
		state = PLAYER_STOPPED;
}

void poke_playback(void)
{
	pthread_cond_signal(&playback_cond);
}

void skip_track(void)
{
	if (state == PLAYER_PLAYING)
		state = PLAYER_END_OF_TRACK;
	else
		puts("we are not playing, no track to skip");
}

void *playback_main(void *userdata)
{
	struct pcm_data pcm;

	(void)userdata;

	while (running)
	{
		puts("sound: starting playback");

		while (state == PLAYER_PLAYING && despotify_get_pcm(session, &pcm) == 0)
			if (ao_play(sound_device, pcm.buf, pcm.len) == 0)
			{
				fputs("sound: critical playback error\n", stderr);
				exit(EXIT_FAILURE);
			}

		if (state != PLAYER_PLAYING)
		{
			fputs("sound: thread suspending\n", stderr);

			next_track();

			pthread_mutex_lock(&playback_mutex);
			while (state != PLAYER_PLAYING && running)
			{
				pthread_cond_wait(&playback_cond, &playback_mutex);
				next_track();
			}
			pthread_mutex_unlock(&playback_mutex);
		}
		else
			fputs("sound: get_pcm failed\n", stderr);
	}

	return NULL;
}

void init_sound(void)
{
	int driver_id;
	ao_sample_format format =
		{
			.bits        = 16,
			.rate        = 44100,
			.channels    = 2,
			.byte_format = AO_FMT_NATIVE
		};

	ao_initialize();

	atexit(shutdown_sound);

	if ((driver_id = ao_default_driver_id()) == -1)
	{
		fputs("sound: could not get default driver for libao\n", stderr);
		exit(EXIT_FAILURE);
	}

	if ((sound_device = ao_open_live(driver_id, &format, NULL)) == NULL)
	{
		fputs("sound: could not open sound driver for live output\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (pthread_create(&playback_thread, NULL, playback_main, NULL) != 0)
	{
		fprintf(stderr, "sound: could not spawn playback thread: %s\n",
			strerror(errno));
		exit(EXIT_FAILURE);
	}

	sound_initialized = 1;
}

void shutdown_despotify(void)
{
	if (session != NULL)
		despotify_exit(session);

	despotify_cleanup();
//	despotify_free(session, true);

	/* queue_foreach(play_queue, despotify_free_track, NULL); */
	queue_destroy(play_queue);

	puts("despotify: exit");
}

static void callback(struct despotify_session *session, int signal, void *data, void *callback_data)
{
	(void)session;
	(void)data;
	(void)callback_data;

	/* Do NOT fuck around with any playback functions in despotify from this
	   function, it will deadlock. despotify_free_track should probably be moved as well. */

	switch (signal)
	{
		case DESPOTIFY_NEW_TRACK:
			puts("new_track event");
			break;

		case DESPOTIFY_END_OF_PLAYLIST:
			state = PLAYER_END_OF_TRACK;
			pthread_cond_signal(&playback_cond);
			break;

		default:
			break;
	}
}

static void
signal_handler(int sig, siginfo_t *info, void *context)
{
	(void)info;
	(void)context;

	printf("signal %d caught\n", sig);

	running = 0;
	pthread_cond_signal(&playback_cond);

	exit(EXIT_SUCCESS);
}

int main(void)
{
	struct sigaction act =
	{
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = &signal_handler,
	};

	/* Block all possible signals while inside signal handler. */
	sigfillset(&act.sa_mask);

	/* Register signal handlers for INT and TERM. */
	if (sigaction(SIGINT, &act, NULL) < 0 ||
	    sigaction(SIGTERM, &act, NULL) < 0)
	{
		perror("sigaction");
		return EXIT_FAILURE;
	}

	if ((play_queue = queue_new(malloc, free)) == NULL)
	{
		perror("queue_new");
		return EXIT_FAILURE;
	}

	init_sound();

	if (!despotify_init())
	{
		fputs("despotify: failed to initialize despotify\n", stderr);
		return EXIT_FAILURE;
	}

	atexit(shutdown_despotify);

	if (!(session = despotify_init_client(callback, NULL, true, true)))
	{
		fprintf(stderr, "error: failed to create session: %s\n",
			despotify_get_error(session));
		return EXIT_FAILURE;
	}

	if (!despotify_authenticate(session, USERNAME, PASSWORD))
	{
		fprintf(stderr, "error: could not log in: %s\n",
			despotify_get_error(session));
		return EXIT_FAILURE;
	}

	rpc_main();

	return EXIT_SUCCESS;
}
