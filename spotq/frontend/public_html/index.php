<?php

header('Content-Type: text/html; charset=UTF-8');
header('Content-Language: en-US');

mb_internal_encoding('UTF-8');

require_once 'xmlrpc.inc.php';
require_once 'util.inc.php';
require_once 'queue.inc.php';

/* Make RPC connection to spotq daemon. */
$rpc    = new XMLRPC('http://192.168.0.2:8000/RPC2');

/* Get and filter request variables. */
$path    = filter_input(INPUT_GET, 'path',   FILTER_UNSAFE_RAW);
$q       = filter_input(INPUT_GET, 'q',      FILTER_UNSAFE_RAW);
$play    = filter_input(INPUT_GET, 'link',   FILTER_UNSAFE_RAW);
$_offset = filter_input(INPUT_GET, 'offset', FILTER_VALIDATE_INT);
$_count  = filter_input(INPUT_GET, 'count',  FILTER_VALIDATE_INT);

@list($page, $param) = explode('/', $path, 2);

/* Set default values. */
$offset = empty($_offset) ? 0  : $_offset;
$count  = empty($_count)  ? 50 : $_count;

/* Convenience function that escapes strings for HTML. */
function e($string)
{
	return htmlspecialchars($string,
	                        ENT_COMPAT, /* Escape only double quotes */
	                        'UTF-8',    /* Use unicode (does nothing) */
	                        TRUE);      /* No double-encode detection */
}

/* Process a track array for display. */
function process_track($track)
{
	global $q, $_offset, $_count;

	if (isset($track['link']))
		$track['play_uri'] = '/link/?' . http_build_query(array
			(
				'q'      => $q,
				'offset' => $_offset,
				'count'  => $_count,
				'link'   => $track['link']
			));

	$track['track']      = utf8_encode($track['track']);
	$track['artist']     = utf8_encode($track['artist']);
	$track['duration']   = duration_format($track['duration']);
	$track['popularity'] = $track['popularity'];
	$track['album']      = utf8_encode($track['album']);

	return $track;
}

switch ($page)
{
	case 'link':
		if (!empty($play))
			$rpc->{'player.link'}($play);

		header('Location: /search/?' . http_build_query(array
			(
				'q'      => $q,
				'offset' => $_offset,
				'count'  => $_count
			)));
		break;

	case 'skip':
		$rpc->{'player.skip'}();

		header('Location: /queue/');
		break;

	case 'queue':
		$response = $rpc->{'player.queue'}(0, $count);

		$results = @array_map('process_track', $response['results']);

		$total = $response['total'];

		require 'queue.tpl.php';
		break;

	case 'search':
	default:
		if (!empty($q))
		{
			$response = $rpc->{'player.search'}($q, $offset, $count);

			$results = @array_map('process_track', $response['results']);

			$total = $response['total'];
			$pages = ceil($total / $count);

			/* Number of clickable links to pages surrounding the current one. */
			$surrounding = 5;

			$first_uri = '/search/?' . http_build_query(array
				(
					'q'      => $q,
					'offset' => 0,
					'count'  => $_count
				));

			$last_uri = '/search/?' . http_build_query(array
				(
					'q'      => $q,
					'offset' => ($pages - 1) * $count,
					'count'  => $_count
				));

			$previous_uri = '/search/?' . http_build_query(array
				(
					'q'      => $q,
					'offset' => max(0, $offset - $count),
					'count'  => $_count
				));

			$next_uri = '/search/?' . http_build_query(array
				(
					'q'      => $q,
					'offset' => max(0, $offset + $count),
					'count'  => $_count
				));

			$current_page = floor($offset / $count);

			for ($i = max(0, $current_page - $surrounding); $i < min($pages, $current_page + $surrounding + 1); $i++)
				$page_uris[$i] = '/search/?' . http_build_query(array
					(
						'q'      => $q,
						'offset' => $i * $count,
						'count'  => $_count
					));

			require 'search.tpl.php';
		}
		else
			require 'empty.tpl.php';
}

