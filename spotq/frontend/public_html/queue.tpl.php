<?php require 'head.tpl.php'; ?>

<?php if (!empty($results)): ?>

<table id="results">
	<thead>
		<tr>
			<th id="add">&nbsp;</th>
			<th id="track">Track</th>
			<th id="artist">Artist</th>
			<th id="duration">Duration</th>
			<th id="popularity">Popularity</th>
			<th id="album">Album</th>
		</tr>
	</thead>
	<tbody>

<?php foreach ($results as $index => $result): ?>

		<tr>
			<?php if ($index == 0): ?>
			<td><b>&raquo;</b></td>
			<?php else: ?>
			<td>&nbsp;</td>
			<?php endif; ?>
			<td><?php echo e($result['track']);      ?></td>
			<td><?php echo e($result['artist']);     ?></td>
			<td><?php echo e($result['duration']);   ?></td>
			<td><?php echo e($result['popularity']); ?>%</td>
			<td><?php echo e($result['album']);      ?></td>
		</tr>

<?php endforeach; ?>

	</tbody>
</table>

<?php else: ?>

<div id="info"><h1>No music</h1><p>The play queue is empty. Add some music!</p></div>

<?php endif; ?>

<?php require 'bottom.tpl.php'; ?>

