<div id="navigation">

	<p>Found <b><?php echo (int)$total; ?></b> tracks.</p>

<?php if ($current_page != 0): ?>
	<a href="<?php echo e($first_uri); ?>">First</a>
	<a href="<?php echo e($previous_uri); ?>">Previous</a>
<?php endif; ?>

<?php if ($current_page - $surrounding > 0) echo '...'; ?>

<?php if (!empty($page_uris)) foreach ($page_uris as $page_number => $page_uri): ?>

	<?php if ($page_number == $current_page): ?>
	<a class="current" href="<?php echo e($page_uri); ?>"><?php echo $page_number + 1; ?></a>
	<?php else: ?>
	<a href="<?php echo e($page_uri); ?>"><?php echo $page_number + 1; ?></a>
	<?php endif; ?>

<?php endforeach; ?>

<?php if ($current_page + $surrounding + 1 < $pages) echo '...'; ?>

<?php if ($current_page + 1 != $pages): ?>
	<a href="<?php echo e($next_uri); ?>">Next</a>
	<a href="<?php echo e($last_uri); ?>">Last</a>
<?php endif; ?>

</div>

