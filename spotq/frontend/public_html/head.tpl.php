<!DOCTYPE html>
<html>
	<head>
		<title>spotq</title>
		<link rel="stylesheet" type="text/css" href="/static/style.css" />
	</head>
	<body>
		<div id="header">
			<h1 id="logo">spotq</h1>
			<form id="search" action="/search/" method="get">
				<input type="text" size="50" id="q" name="q" value="<?php echo htmlspecialchars($q); ?>" />
				<input type="submit" class="button" value="Search" />
			</form>

			<span class="button<?php echo $page == 'queue' ? ' current' : ''; ?>"><a href="/queue/">Queue</a></span>
			<span class="button"><a href="/skip/">Skip song</a></span>
		</div>
		<div id="content">

