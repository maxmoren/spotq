<?php require 'head.tpl.php'; ?>

<div id="info">
	<h1>Search for music</h1>

	<p>Enter search terms into the search box above to find and add music to the play queue.</p>
		Examples:
		<ul>
			<li>artist:"Martika" track:"Toy Soldiers"</li>
			<li>Dr Bombay Calcutta</li>
		</ul>
	</p>
	<p>See the <a href="http://www.spotify.com/uk/about/features/searching-for-music/">Spotify manual</a> for more information about the search features.</p>
</div>

<?php require 'bottom.tpl.php'; ?>

