#ifndef _SPOTQ_H
#define _SPOTQ_H

#include <pthread.h>
#include <libspotify/api.h>

#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

struct track_info
{
	char *artist;
	char *album;
	char *track;
	char *link;
	int popularity;
	int duration;
};

struct track
{
	int loaded;
	struct track_info *track_info;
	sp_track *track;
};

struct search
{
	char *query;
	char *dym;
	int offset;
	int count;
	int tot_tracks;
	int num_tracks;
	struct track_info *tracks;
	int error;
	int complete;
	pthread_cond_t *cond;
};

struct link
{
	char *uri;
};

void notify_main(void);

extern struct queue *link_queue;
extern struct queue *search_queue;
extern struct queue *play_queue;

extern int g_end_of_track;

#endif

