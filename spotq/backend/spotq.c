#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED
#define _POSIX_C_SOURCE	199309L
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <libspotify/api.h>

#include "spotq.h"
#include "appkey.h"
#include "queue.h"
#include "sound.h"
#include "rpc.h"

/* 16 MiB cache */
#define CACHE_SIZE 16777216

/* High bitrate */
#define HQ

/* Action queues. */
struct queue *link_queue   = NULL;
struct queue *search_queue = NULL;
struct queue *play_queue   = NULL;

/* Global status. */
static int g_exit      = FALSE;
static int g_logged_in = FALSE;
static int g_playing   = FALSE;

int g_end_of_track = FALSE;

void
destroy_track_info(struct track_info *ti)
{
	free(ti->artist);
	free(ti->album);
	free(ti->track);
	free(ti->link);
	free(ti);
}

void
extract_track_info(sp_track *tr, struct track_info *ti)
{
	char uri[1024];
	sp_album  *album  = sp_track_album(tr);
	sp_artist *artist = sp_track_artist(tr, 0);
	sp_link   *link   = sp_link_create_from_track(tr, 0);

	sp_link_as_string(link, uri, sizeof(uri));

	ti->artist        = strdup(sp_artist_name(artist));
	ti->album         = strdup(sp_album_name(album));
	ti->track         = strdup(sp_track_name(tr));
	ti->link          = strdup(uri);
	ti->duration      = sp_track_duration(tr);
	ti->popularity    = sp_track_popularity(tr);

	sp_link_release(link);
}

static void
get_audio_buffer_stats(sp_session *session,
                       sp_audio_buffer_stats *stats)
{
	(void)session;

	sound_stats(&stats->samples, &stats->stutter);
}

static void
search_complete(sp_search *results, void *data)
{
	struct search *search = (struct search *)data;

	int num_tracks = sp_search_num_tracks(results);

	if (num_tracks > 0)
	{
		search->tracks = malloc(sizeof(struct track_info) * num_tracks);

		if (search->tracks == NULL)
		{
			search->error      = TRUE;
			search->complete   = TRUE;
			search->num_tracks = 0;
			search->tracks     = NULL;
			search->dym        = NULL;

			fputs("error: malloc failed\n", stderr);

			sp_search_release(results);

			pthread_cond_broadcast(search->cond);

			return;
		}
	}
	else
		search->tracks = NULL;

	for (int i = 0; i < num_tracks; i++)
		extract_track_info(sp_search_track(results, i), &search->tracks[i]);

	const char *dym = sp_search_did_you_mean(results);

	if (dym != NULL)
		search->dym = strdup(dym);

	search->error      = FALSE;
	search->complete   = TRUE;
	search->tot_tracks = sp_search_total_tracks(results);
	search->num_tracks = num_tracks;
	pthread_cond_broadcast(search->cond);

	sp_search_release(results);
}

pthread_mutex_t notify_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t notify_cond = PTHREAD_COND_INITIALIZER;

void
notify_main(void)
{
	pthread_cond_signal(&notify_cond);
}

static void
signal_handler(int sig, siginfo_t *info, void *context)
{
	(void)info;
	(void)context;

	printf("signal %d caught\n", sig);

	g_exit = TRUE;
	notify_main();
}

int
main(void)
{
	struct sigaction act =
	{
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = &signal_handler,
	};

	/* Block all possible signals while inside signal handler. */
	sigfillset(&act.sa_mask);

	/* Register signal handlers for INT and TERM. */
	if (sigaction(SIGINT, &act, NULL) < 0 ||
	    sigaction(SIGTERM, &act, NULL) < 0)
	{
		perror("sigaction");
		return EXIT_FAILURE;
	}

	sound_init();
	rpc_init();

	while (!g_exit)
	{
		
	}

	puts("normal exit");

	return EXIT_SUCCESS;
}

