#ifndef _TEST_H
#define _TEST_H

#include <stdio.h>

#define TEST(expr) \
((expr) \
? fprintf(stderr, "%4d \033[32m    OK\033[0m: %s\n", __LINE__, __STRING(expr)) \
: fprintf(stderr, "%4d \033[31mFAILED\033[0m: %s\n", __LINE__, __STRING(expr)))

#endif

