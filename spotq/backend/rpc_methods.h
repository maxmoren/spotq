#ifndef _RPC_METHODS_H
#define _RPC_METHODS_H

#include <xmlrpc-c/base.h>

xmlrpc_value *player_search(xmlrpc_env *const, xmlrpc_value *const, void *const, void *const);
xmlrpc_value *player_link(xmlrpc_env *const, xmlrpc_value *const, void *const, void *const);
xmlrpc_value *player_queue(xmlrpc_env *const, xmlrpc_value *const, void *const, void *const);
xmlrpc_value *player_skip(xmlrpc_env *const, xmlrpc_value *const, void *const, void *const);

#endif

