#define _POSIX_C_SOURCE 200809L
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/abyss.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/server_abyss.h>

#include "rpc_methods.h"

static xmlrpc_server_abyss_t *g_serverP;
static xmlrpc_registry       *g_registryP;
static xmlrpc_env             g_env;
static pthread_t              g_thread;

static void
dieIfFailed(const char * const description)
{
    if (g_env.fault_occurred)
	{
        fprintf(stderr, "error: %s failed. %s\n", description, g_env.fault_string);
        exit(1);
    }
}

void *rpc_main(void *data)
{
	(void)data;

	xmlrpc_server_abyss_run_server(&g_env, g_serverP);
	dieIfFailed("xmlrpc_server_abyss_run_server");

	return NULL;
}

void rpc_shutdown(void)
{
    xmlrpc_server_abyss_terminate(&g_env, g_serverP);
    xmlrpc_server_abyss_global_term();

	pthread_join(g_thread, NULL);

    xmlrpc_registry_free(g_registryP);
    xmlrpc_env_clean(&g_env);
}

void rpc_init(void)
{
    struct xmlrpc_method_info3 const methodInfo[] =
	{
		{
		    .methodName     = "player.search",
		    .methodFunction = &player_search
		},
		{
		    .methodName     = "player.link",
		    .methodFunction = &player_link
		},
		{
			.methodName     = "player.queue",
			.methodFunction = &player_queue
		},
		{
			.methodName     = "player.skip",
			.methodFunction = &player_skip
		},
    };

	/* Initialize environment. */
    xmlrpc_env_init(&g_env);

	/* Create registry of methods. */
	g_registryP = xmlrpc_registry_new(&g_env);
    dieIfFailed("xmlrpc_registry_new");

	/* Initialize abyss gobally. */
	xmlrpc_server_abyss_global_init(&g_env);
	dieIfFailed("xmlrpc_server_abyss_global_init");

    xmlrpc_server_abyss_parms serverparm =
	{
		.config_file_name = NULL,
		.registryP        = g_registryP,
		.port_number      = 8000
	};

	for (int i = 0; i < 4; i++)
	{
		xmlrpc_registry_add_method3(&g_env, g_registryP, &methodInfo[i]);
		dieIfFailed("xmlrpc_registry_add_method2");
	}

    xmlrpc_server_abyss_create(&g_env, &serverparm, XMLRPC_APSIZE(port_number),
		&g_serverP);
    dieIfFailed("xmlrpc_server_abyss_create");

	pthread_create(&g_thread, NULL, rpc_main, NULL);
}

