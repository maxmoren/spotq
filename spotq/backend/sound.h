#ifndef _SOUND_H
#define _SOUND_H

int sound_init(void);
int sound_samples(void);
void sound_stats(int *, int *);
size_t sound_buffer(const void *, size_t);
void sound_shutdown(void);

#endif

