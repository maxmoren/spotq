#define __need_size_t
#define __need_NULL
#include <stddef.h>
#include <pthread.h>

struct element
{
	void *data;
	struct element *next;
};

struct queue
{
	pthread_mutex_t mutex;
	struct element *first, *last, *current;
	void *(*alloc)(size_t);
	void (*dealloc)(void *);
	size_t count;
};

struct queue *queue_new(void *(*alloc)(size_t), void (*dealloc)(void *))
{
	struct queue *q;

	if ((q = alloc(sizeof(struct queue))) == NULL)
		return NULL;

	q->alloc   = alloc;
	q->dealloc = dealloc;
	q->first   = NULL;
	q->last    = NULL;
	q->current = NULL;
	q->count   = 0;

	if (pthread_mutex_init(&q->mutex, NULL) != 0)
	{
		dealloc(q);
		return NULL;
	}

	return q;
}

void queue_destroy(struct queue *q)
{
	struct element *e, *next;

	for (e = q->first; e != NULL; e = next)
	{
		next = e->next;
		q->dealloc(e);
	}

	pthread_mutex_destroy(&q->mutex);

	q->dealloc(q);
}

int queue_enqueue(struct queue *q, void *data)
{
	struct element *e;

	if((e = q->alloc(sizeof(struct element))) == NULL)
		return 0;

	e->data = data;
	e->next = NULL;

	pthread_mutex_lock(&q->mutex);

	if(q->last != NULL)
		q->last->next = e;

	if(q->first == NULL)
		q->first = e;

	q->last = e;
	q->count++;

	pthread_mutex_unlock(&q->mutex);

	return 1;
}

void *queue_peek(struct queue *q)
{
	struct element *e;

	pthread_mutex_lock(&q->mutex);
	e = q->first;
	pthread_mutex_unlock(&q->mutex);

	if (e == NULL)
		return NULL;

	return e->data;
}

void *queue_dequeue(struct queue *q)
{
	struct element *e;
	void *data;

	pthread_mutex_lock(&q->mutex);

	if (q->first == NULL)
	{
		pthread_mutex_unlock(&q->mutex);
		return NULL;
	}

	e        = q->first;
	data     = e->data;
	q->first = e->next;

	if (e == q->current)
		q->current = NULL;

	if(e->next == NULL)
		q->last = NULL;

	q->dealloc(e);

	q->count--;

	pthread_mutex_unlock(&q->mutex);

	return data;
}

size_t queue_count(struct queue *q)
{
	size_t count;

	pthread_mutex_lock(&q->mutex);
	count = q->count;
	pthread_mutex_unlock(&q->mutex);

	return count;
}

void queue_foreach(struct queue *q, void (*func)(size_t, void *, void *), void *userdata)
{
	size_t i = 0;
	struct element *e;

	pthread_mutex_lock(&q->mutex);

	for (e = q->first; e; e = e->next)
		func(i++, e->data, userdata);

	pthread_mutex_unlock(&q->mutex);
}

